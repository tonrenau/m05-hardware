vlsConsultant el sistema operatiu amb les ordres estudiades identificar els components
obtenint la següent informació:

● CPU: Model i marca. Velocitat. Número de cpus. Permet virtualització?

# lscpu
Model name:                      Intel(R) Core(TM) i7-9700K CPU @ 3.60GHz
velocitat: 			 @ 3.60GHz
CPU(s):                          8
Virtualization:                  VT-x


● Memòria: Quantitat RAM total. Número de bancs de memòria de la placa, quants
estan ocupats. Quines pastilles de memòria hi ha? Quantes, de quina capacitat i
de quina velocitat i característiques.

# lsmem 
Total online memory:      16G

Dos bancs de memoria per cada linea: 
0x0000000000000000-0x00000000cfffffff  3.3G online       yes   0-25
0x0000000100000000-0x000000042fffffff 12.8G online       yes 32-133


# dmidecode -t  memory

● Targeta de xarxa: Quin model de targeta de xarxa hi ha. Amb quin ample de
banda màxim? Quin és el driver o mòdul del kernel?. N’hi ha més d’una? És USB
o PCI?

# lspci 

# lspci -v -s 00:1f.6 (mostra info detallada de la linea)
00:1f.6 Ethernet controller: Intel Corporation Ethernet Connection (2) I219-V
	DeviceName: Onboard - Ethernet
	Subsystem: Gigabyte Technology Co., Ltd Ethernet Connection (2) I219-V
	Flags: bus master, fast devsel, latency 0, IRQ 131, IOMMU group 16
	Memory at f7100000 (32-bit, non-prefetchable) [size=128K]
	Capabilities: [c8] Power Management version 3
	Capabilities: [d0] MSI: Enable+ Count=1/1 Maskable- 64bit+
	Capabilities: [e0] PCI Advanced Features
	Kernel driver in use: e1000e
	Kernel modules: e1000e
# ip l ( veus els noms de la targeta de xaraxa )

# ethtool eno1 (info targreta de xarxa dels noms extrets abns) 


● Targeta de vídeo: Quin model de targeta de vídeo és. s’indiquen prestacions?.
Quin és el mòdul del kernel usat de driver?.

#lspci

#lspci -v -s 00:02.0
00:02.0 VGA compatible controller: Intel Corporation CoffeeLake-S GT2 [UHD Graphics 630] (rev 02) (prog-if 00 [VGA controller])
	DeviceName: Onboard - Video
	Subsystem: Gigabyte Technology Co., Ltd UHD Graphics 630 (Desktop 9 Series)
	Flags: bus master, fast devsel, latency 0, IRQ 143, IOMMU group 1
	Memory at f6000000 (64-bit, non-prefetchable) [size=16M]
	Memory at e0000000 (64-bit, prefetchable) [size=256M]
	I/O ports at f000 [size=64]
	Expansion ROM at 000c0000 [virtual] [disabled] [size=128K]
	Capabilities: [40] Vendor Specific Information: Len=0c <?>
	Capabilities: [70] Express Root Complex Integrated Endpoint, MSI 00
	Capabilities: [ac] MSI: Enable+ Count=1/1 Maskable- 64bit-
	Capabilities: [d0] Power Management version 2
	Capabilities: [100] Process Address Space ID (PASID)
	Capabilities: [200] Address Translation Service (ATS)
	Capabilities: [300] Page Request Interface (PRI)
	Kernel driver in use: i915
	Kernel modules: i915


● Quin tipus de mouse s’utilitza? Qui és el fabricant i quin és el mòdul de kernel
usat com a driver.

# lsusb ( agafes el bus (numero d'identificador))
# lsusb -v -d 046d:c077 



● Anomena algun altre dispositiu PCI que tinguis.


● Anomena algun altre dispositiu USB que tinguis.


● Descriu el fabricant i la capacitat del disc dur. Quin tipus de disc dur és,
rotacional o SSD?. Quants particins té?.

# fdisk -l
(SSD -> almegamatsematge mes rapid, de ordres de magintud mes rapid)


● Quina Bios utilitza el teu sistema. de quin fabricant i quina versió?

# dmidecode -t BIOS


● Quin tipus de chasis té el teu sistema? Com pots obtenir aquesta informació?

# dmidecode -t chassis
dmidecode 3.3
Getting SMBIOS data from sysfs.
SMBIOS 3.1.1 present.

Handle 0x0003, DMI type 3, 22 bytes
Chassis Information
	Manufacturer: Default string
	Type: Desktop
	Lock: Not Present
	Version: Default string
	Serial Number: Default string
	Asset Tag: Default string
	Boot-up State: Safe
	Power Supply State: Safe
	Thermal State: Safe
	Security Status: None
	OEM Information: 0x00000000
	Height: Unspecified
	Number Of Power Cords: 1
	Contained Elements: 0
	SKU Number: Default string









<dmidecode -t ch
Invalid type keyword: ch
Valid type keywords are:
  bios
  system
  baseboard
  chassis
  processor
  memory
  cache
  connector
  slot>
(tots els dmidecode) 
  
