# LPIC-1


## 102.5-Use_RPM_and_YUM_package_management


### Exercices


1. Install the packages geany and pwgen.
# dnf install geany
# dnf install pwgen

2. List the pwgen package contents.
# rpm -ql pwgen

3. Show the geany package info.
# dnf info  geany

# rpm -qi geany

4. Show the openldap-clients package info (not installed).
# dnf info openldap-clients


5. The file /etc/fstab to which package belongs?
# rpm -qf /etc/fstab

6. The command /usr/bin/date to which package belongs?
# rpm -qf /usr/bin/date

7. Download the package (not install) nmap.
# dnf download nmap

8. Install the local package nmap. Reinstall if necessary.
# sudo dnf install nmap

# 
9. List all the repos installed.
# yum repolist

10. Show the fedora.repo file configuration.
# cat /etc/yum.repos.d/fedora.repos
11. Install docker for CentOS. (no fer)

---

**Tip for centos version**

```
# The correct and easy way to migrate from CentOS Linux 8 to CentOS Stream 8 is to run:
dnf --disablerepo '*' --enablerepo=extras swap centos-linux-repos centos-stream-repos
dnf distro-sync
```
```
sudo sed -i -e "s/mirrorlist=/#mirrorlist=/g" /etc/yum.repos.d/CentOS-*
sudo sed -i -e "s|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g" /etc/yum.repos.d/CentOS-*

sudo dnf update
```

