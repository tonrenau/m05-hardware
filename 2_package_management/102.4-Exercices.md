# LPIC-1


## 102.4-Use_debian_package_management


### Exercices


1. Which command actualizes the apt cache information?

#apt-get update

2. Install the geany and pwgen packages

#apt-get install pwgen geany

3. List all installed packages.

#dpkg -l

4. Show the geany package information.

#dpkg -s geany

5. Show the contents of the pwgen package.

#dpkg -L pwgen

6. Which package contains the /usr/bin/passwd file?

#dpkg -S /usr/bin/passwd 

7. Reconfigure the package tzdata.

#dpkg-reconfigure tzdata

8. Search for packages with the name ldap-utils.

#dpkg -S ldap-untils

9. Show information about the ldap-utils package.

#dpkg -s ldap-untils
#apt-cache search ldap

10. Show the dependencies of the ldap-utils package.

# apt-cache depends ldap-utils

11. Show the file containing the repository configurations.

#vim /etc/apt/sources.list 

12. Purge the package geany.

# dpkg -P geany

13. Local download the geany package and install it.

#apt-get download geany
# sudo dpkg -i Downloads/geany_1.29-1_amd64.deb 
if 
dpkg -P geany 
sudo apt-get --fix-broken install ./Downloads/geany_1.29-1_amd64.deb

14. Install the repositories contrib and non-free.

15. Install docker for Debian.



